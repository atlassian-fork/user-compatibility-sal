package com.atlassian.sal.usercompatibility;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This file was directly copied from SAL
 *
 * Represents an identifier that uniquely identifies a user for the duration of its existence.
 */
public final class UserKey implements Serializable
{
    private final String userkey;

    /**
     * Default constructor: builds a UserKey from its string representation.
     *
     * @param userkey the string representation of a UserKey. Must not be null.
     */
    public UserKey(final String userkey)
    {
        checkNotNull(userkey, "userkey");
        this.userkey = userkey;
    }

    /**
     * Returns a string representation of the current key.
     *
     * @return a string representation of the current key
     */
    public String getStringValue()
    {
        return userkey;
    }

    @Override
    public String toString()
    {
        return getStringValue();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        UserKey userKey = (UserKey) o;

        if (userkey != null ? !userkey.equals(userKey.userkey) : userKey.userkey != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return userkey != null ? userkey.hashCode() : 0;
    }

    //
    // Factory methods
    //

    /**
     * Builds a {@link UserKey} object from a long id. The id will be converted to a String first.
     *
     * @param userId a user ID
     * @return a {@link UserKey} object
     */
    public static UserKey fromLong(long userId)
    {
        return new UserKey(String.valueOf(userId));
    }
}
